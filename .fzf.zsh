# Setup fzf
# ---------
if [[ ! "$PATH" == */home/robin/.config/nvim/pack/bundle/opt/fzf/bin* ]]; then
  export PATH="${PATH:+${PATH}:}/home/robin/.config/nvim/pack/bundle/opt/fzf/bin"
fi

# Auto-completion
# ---------------
[[ $- == *i* ]] && source "/home/robin/.config/nvim/pack/bundle/opt/fzf/shell/completion.zsh" 2> /dev/null

# Key bindings
# ------------
source "/home/robin/.config/nvim/pack/bundle/opt/fzf/shell/key-bindings.zsh"
