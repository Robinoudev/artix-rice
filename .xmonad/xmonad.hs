--- IMPORTS
import XMonad
import Data.Monoid
import System.Exit

--- ACTIONS
import XMonad.Actions.WindowGo

--- HOOKS
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.InsertPosition
import XMonad.Hooks.DynamicLog

--- LAYOUTS
import XMonad.Layout.Spacing
import XMonad.Layout.ToggleLayouts
import XMonad.Layout.NoBorders
import XMonad.Layout.WindowNavigation

--- UTILS
import XMonad.Util.Run
import XMonad.Util.EZConfig

import qualified XMonad.StackSet as W
import qualified Data.Map        as M

-- The preferred terminal program, which is used in a binding below and by
-- certain contrib modules.
--
myTerminal      = "st"
myBrowser       = "firefox"

-- Whether focus follows the mouse pointer.
myFocusFollowsMouse :: Bool
myFocusFollowsMouse = True

-- Whether clicking on a window to focus also passes the click to the window
myClickJustFocuses :: Bool
myClickJustFocuses = False

-- Width of the window border in pixels.
--
myBorderWidth   = 2

-- modMask lets you specify which modkey you want to use. The default
-- is mod1Mask ("left alt").  You may also consider using mod3Mask
-- ("right alt"), which does not conflict with emacs keybindings. The
-- "windows key" is usually mod4Mask.
--
myModMask       = mod4Mask

-- The default number of workspaces (virtual screens) and their names.
-- By default we use numeric strings, but any string may be used as a
-- workspace name. The number of workspaces is determined by the length
-- of this list.
--
-- A tagging example:
--
-- > workspaces = ["web", "irc", "code" ] ++ map show [4..9]
--

xmobarEscape = concatMap doubleLts
  where doubleLts '<' = "<<"
        doubleLts x   = [x]

myWorkspaces :: [String]
myWorkspaces = clickable . (map xmobarEscape) $ ["1","2","3","4","5","6","7","8","9"]
  where
    clickable l = [ "<action=xdotool key super+" ++ show (n) ++ ">" ++ "<fn=2>" ++ ws ++ "</fn>" ++ "</action>" |
                             (i,ws) <- zip [1..9] l,
                            let n = i ]

-- Border colors for unfocused and focused windows, respectively.
--
myNormalBorderColor  = "#dddddd"
myFocusedBorderColor = "#ff0000"

windowCount :: X (Maybe String)
windowCount = gets $ Just . show . length . W.integrate' . W.stack . W.workspace . W.current . windowset

------------------------------------------------------------------------
-- Key bindings. Add, modify or remove key bindings here.
--
myKeys = \conf -> mkKeymap conf $
  [ ("M-<Return>", spawn(myTerminal))
  , ("M-p", spawn "dmenu_run")
  , ("M-q", kill)
  , ("M-<Space>", sendMessage NextLayout)
  , ("M-S-<Space>", setLayout $ XMonad.layoutHook conf)
  , ("M-S-n", refresh)
  , ("M-n", spawn "st -e newsboat")
  , ("M-e", spawn "st -e neomutt")
  , ("M-j", sendMessage $ Go D)
  , ("M-k", sendMessage $ Go U)
  , ("M-h", sendMessage $ Go L)
  , ("M-l", sendMessage $ Go R)
  , ("M-m", windows W.focusMaster)
  , ("M-S-m", windows W.swapMaster)
  , ("M-S-j", sendMessage $ Swap D)
  , ("M-S-k", sendMessage $ Swap U)
  , ("M-S-h", sendMessage Shrink)
  , ("M-S-l", sendMessage Expand)
  , ("M-t", withFocused $ windows . W.sink)
  , ("M-,", sendMessage (IncMasterN 1))
  , ("M-.", sendMessage (IncMasterN (-1)))
  , ("M-S-b", spawn myBrowser)
  , ("M-b", sequence_ [
      toggleScreenSpacingEnabled, toggleWindowSpacingEnabled, sendMessage ToggleStruts
      ])
  , ("M-S-q", io (exitWith ExitSuccess))
  , ("M-S-r", spawn "xmonad --recompile; xmonad --restart")
  ]

  ++
  -- Workspace bindings
  [ (m ++ i, windows $ f j)
      | (i, j) <- zip (map show [1..9]) (XMonad.workspaces conf)
      , (m, f) <- [("M-", W.greedyView), ("M-S-", W.shift)]
  ]

  ++
  -- Monitor bindings
  [ (m ++ key, screenWorkspace sc >>= flip whenJust (windows . f))
      | (key, sc) <- zip ["u", "i", "o"] [0..]
      , (m, f) <- [("M-", W.view), ("M-S-", W.shift)]
  ]

------------------------------------------------------------------------
-- Mouse bindings: default actions bound to mouse events
--
myMouseBindings (XConfig {XMonad.modMask = modm}) = M.fromList $

    -- mod-button1, Set the window to floating mode and move by dragging
    [ ((modm, button1), (\w -> focus w >> mouseMoveWindow w
                                       >> windows W.shiftMaster))

    -- mod-button2, Raise the window to the top of the stack
    , ((modm, button2), (\w -> focus w >> windows W.shiftMaster))

    -- mod-button3, Set the window to floating mode and resize by dragging
    , ((modm, button3), (\w -> focus w >> mouseResizeWindow w
                                       >> windows W.shiftMaster))

    -- you may also bind events to the mouse scroll wheel (button4 and button5)
    ]

------------------------------------------------------------------------
-- Layouts:

-- You can specify and transform your layouts by modifying these values.
-- If you change layout bindings be sure to use 'mod-shift-space' after
-- restarting (with 'mod-q') to reset your layout state to the new
-- defaults, as xmonad preserves your old layout settings by default.
--
-- The available layouts.  Note that each layout is separated by |||,
-- which denotes layout choice.
--
myLayout = spacingRaw False (Border 5 5 5 5) True (Border 15 5 5 5) True
           $ avoidStruts
           $ toggleLayouts (noBorders Full)
           $ smartBorders
           $ windowNavigation
           $ basicLayout
    where
      basicLayout   = tiled ||| Full
      tiled         = Tall nmaster delta ratio
      nmaster       = 1
      ratio         = 1/2
      delta         = 3/100

------------------------------------------------------------------------
-- Window rules:

-- Execute arbitrary actions and WindowSet manipulations when managing
-- a new window. You can use this to, for example, always float a
-- particular program, or have a client always appear on a particular
-- workspace.
--
-- To find the property name associated with a program, use
-- > xprop | grep WM_CLASS
-- and click on the client you're interested in.
--
-- To match on the WM_NAME, you can use 'title' in the same way that
-- 'className' and 'resource' are used below.
--
myManageHook = insertPosition End Newer <+> -- Push new window to end of stack and focus Newer window
                composeAll
    [ className =? "MPlayer"        --> doFloat
    , className =? "Gimp"           --> doFloat
    , resource  =? "desktop_window" --> doIgnore
    , resource  =? "kdesktop"       --> doIgnore
    , (className =? "firefox" <&&> resource =? "Dialog") --> doFloat
    , (className =? "brave" <&&> resource =? "Dialog") --> doFloat ]

------------------------------------------------------------------------
-- Event handling

-- * EwmhDesktops users should change this to ewmhDesktopsEventHook
--
-- Defines a custom handler function for X Events. The function should
-- return (All True) if the default handler is to be run afterwards. To
-- combine event hooks use mappend or mconcat from Data.Monoid.
--
myEventHook = mempty

------------------------------------------------------------------------
-- Status bars and logging

-- Perform an arbitrary action on each internal state change or X event.
-- See the 'XMonad.Hooks.DynamicLog' extension for examples.
--
myLogHook = return ()
------------------------------------------------------------------------
-- Startup hook

-- Perform an arbitrary action each time xmonad starts or is restarted
-- with mod-q.  Used by, e.g., XMonad.Layout.PerWorkspace to initialize
-- per-workspace layout choices.
--
-- By default, do nothing.
myStartupHook = return ()

------------------------------------------------------------------------
-- Now run xmonad with all the defaults we set up.

-- Run xmonad with the settings you specify. No need to modify this.
--
main :: IO ()
main = do
  xmproc <- spawnPipe "xmobar -x 0 /home/robin/.config/xmobar/xmobarrc"
  xmonad $ docks def {
      -- simple stuff
        terminal           = myTerminal,
        focusFollowsMouse  = myFocusFollowsMouse,
        clickJustFocuses   = myClickJustFocuses,
        borderWidth        = myBorderWidth,
        modMask            = myModMask,
        workspaces         = myWorkspaces,
        normalBorderColor  = myNormalBorderColor,
        focusedBorderColor = myFocusedBorderColor,

      -- key bindings
        keys               = myKeys,
        mouseBindings      = myMouseBindings,

      -- hooks, layouts
        layoutHook         = myLayout,
        manageHook         = myManageHook,
        handleEventHook    = myEventHook,
        startupHook        = myStartupHook,
        logHook = myLogHook <+> dynamicLogWithPP xmobarPP
                  { ppOutput = hPutStrLn xmproc
                  , ppCurrent = xmobarColor "#c3e88d" "" . wrap "[" "]"
                  , ppVisible = xmobarColor "#c3e88d" ""
                  , ppHidden = xmobarColor "#82AAFF" "" . wrap "*" ""
                  , ppHiddenNoWindows = xmobarColor "#F07178" ""
                  , ppTitle   = xmobarColor "#d0d0d0"  "" . shorten 60
                  , ppSep =  "<fc=#666666> | </fc>"
                  , ppUrgent  = xmobarColor "#C45500" "" . wrap "!" "!"
                  , ppExtras  = [windowCount]
                  , ppOrder  = \(ws:l:t:ex) -> [ws,l]++ex++[t]
                  }
        } `additionalKeys`
        []
